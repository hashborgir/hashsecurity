+++
title="Rules"
author="Hash"
date = "2021-09-06T12:55:22-06:00"
+++

# Channel Vision

The purpose of #security on Libera is to provide a fun and friendly medium for new and hardcore security enthusiasts, hackers, cybersecurity students, teachers, and professionals alike seeking help, advice and constructive discussion on security related topics.

The channel is for **all** levels of user experience, including none.

# Rules: #security on Libera


* English only, please.
* Be helpful, kind, and courteous, and try to provide accurate information.
* Be nice. Abusive, insulting, derogatory, condescending and demeaning language use towards channel users is not allowed.
* No flooding, no all-caps, no intentional annoyance.
* Stay on topic and stick to the discussion of Secureity related topics.

# Catalyst Guidelines

The “catalyst” role is critical to the #security community and an essential building block of effective communities. No one is required to be a catalyst, but the users who perform this role ensure the smooth and efficient functioning of the network.

Chat platforms does not automatically produce a stable culture of cooperative effort. Even in cases where cooperation is intended, misunderstandings and personality incompatibilities can result in an extremely chaotic and hostile environment. Catalysts help prevent and resolve misunderstanding, calm the waters when users have difficulties dealing with each other and provide examples of constructive behavior in environments where such behavior might not otherwise be the norm.

Catalysts try to resolve problems, not through the use of authority and special privilege, but by fostering consensus, gently nudging participants in the direction of more appropriate behavior and by generally reducing the level of confrontation rather than confronting users with problems.

Community staff may be catalysts and, indeed, are encouraged to take on that role. Communities which recognize the importance of the catalyst role will foster more effective coordination of effort. An important characteristic of successful catalysts is the infrequency with which they wear authority or invoke special privilege.

## An effective catalyst is:

-   **Relaxed.**  To keep things calm, you yourself must be calm. Learn the skills of staying genuinely relaxed. Know your limitations; when you can’t handle a problem situation calmly, get calmer heads involved.

-   **Open-minded.**  It is easy to make assumptions about other people’s motivations. When you decide someone is behaving maliciously, you have made an assumption about their motivation which may be difficult to disprove. Try to make your assumptions about other people’s motivations as positive as possible.

-   **Responsible.**  Chatting and collaboration is a group activity with a strong need for responsible individual behavior. Rumors, innuendo and gossip can derail discussions and ruin reputations. If everybody knows something is true, who is “everybody?” Did the person you are talking to get their information from documented, factual sources, or is it hearsay? If you cannot be sure of the answer to those questions, should you be passing on what they have said?

-   **Unobtrusive.**  It is not necessary to invoke authority to help solve a problem, and far better if you do not. Look for an opportunity to nudge the situation into a more productive track. Do not criticize the user if a quiet change of subject, or a private conversation on a completely different topic, can help make the problem fade away.

-   **Realistic.**  Accept the personalities of users and concentrate on problem resolution. Do not expect people to suddenly change their personalities to make problem resolution easier.

-   **Careful.**  Everything you say will be interpreted by the users with whom you interact. Consider how your remarks will be interpreted before you make them. Make sure the message you convey is the one you intend.

-   **Attentive.**  Understand the situation you have walked into before you act. Question your assumptions. Look for signs that you may have misinterpreted the situation, in order to avoid causing difficulties for a user who did not create the problem.

-   **Minimalist.**  Do not do more than you need to in order to resolve a problem. A problem scene is often the wrong time and place to set policy. Concentrate on the resolution, and on collecting information you can think about later.

-   **Courteous.**  Even under time pressure, courtesy costs little and impresses people a lot. It is not about whether working with the person is easy or difficult; it is about setting the right tone.

-   **Cooperative.**  Look for opportunities to get people involved in the resolution of their own and others’ problems.

-   **Someone with an internal locus of control.**  Catalysts concentrate on solving problems, not bestowing blame. Treat the situation as the problem, accept the users for who they are and try to figure out how best to help resolve the difficulty.

-   **A user.**  Remember that you are not in charge. Everybody runs their own little corner of the world. Let them do the job they are capable of. Just help the process along as unobtrusively as possible. Other catalysts are users as well, and nobody is perfect.

We are all just here to do our best to keep things running well.
