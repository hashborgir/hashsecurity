+++
title="#security Staff"
author="Hash"
date = "2021-09-06T12:55:22-06:00"
+++

# Founder
**electr0n**
<img src="/img/hacker1.png" style="float:left; padding: 10px;">
founded the original #security channel on the original Freenode IRC network. Due to the recent takeovers, he's decided to shift the community to Libera.chat.

<hr>

# Contributors

**njan**
<img src="/img/hacker4.png" style="float:left; padding: 10px;">
registered the #security channel on freenode and is also libera staff.

<br>

**skill**
<img src="/img/hacker4.png" style="float:left; padding: 10px;">
Overall security nerd, historically focusing on product security but now run security/IT/compliance for a public company. Experience with how to drive devsecops culture across large organizations and spend my time now helping smaller organizations build their programs and work with folks on getting into the field


**Hydragyrum**
<img src="/img/hacker4.png" style="float:left; padding: 10px;">
Channel Moderator and curator of infosec books and resources

<br>

**Hash**
<img src="/img/hacker4.png" style="float:left; padding: 10px;">
runs the *secbot* IRC bot, as well as build and manage the hashsecurity.net website. Currently studying for a Computer Science bachelors degree in Cybersecurity.
