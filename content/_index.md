+++
title="Welcome to #Security"
description = "This is the official site for the #security community on Libera.chat IRC Network."
+++

# Welcome to the #security community!

This is the official site for the #security community on [Libera.chat IRC Network.](https://libera.chat/)

-   [Basics of IRC](https://libera.chat/guides/basics)
-   [Frequently Asked Questions](https://libera.chat/guides/faq)
-   [Choosing an IRC client](https://libera.chat/guides/clients)

The hashsecurity community is a global community for general InfoSec support. We provide help and support for any security related issues and discussions, and aim to answer any questions you might have about any aspect of security. Discussion about the various aspects of Free/Open Source security software is also encouraged.

Our IRC community focuses mostly around on-topic discussions revolving around Security related topics such as networking, operating systems, virtualization, programming, code auditing, physical, and generally all aspects of Information Security.

Please be sure to read the various community rules before joining as there may be differences based on the platform and culture.

We hope you enjoy your stay!

