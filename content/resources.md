+++
title="Infosec & Cybersecurity Educational Resources"
author="Hash"
date = "2021-09-06T12:55:22-06:00"
+++


# Infosec & Cybersecurity Educational Resources

* [Cyber Security Degrees](https://cybersecuritydegrees.org/) – lists of scholarships, degree programs, and certifications in the USA

* [ENISA Training Resources](https://www.enisa.europa.eu/topics/trainings-for-cybersecurity-specialists/online-training-material) – educational resources

* [Hacker101](https://www.hacker101.com/) – free, open-source video lessons on web security

* [Open Security Training](http://www.opensecuritytraining.info/) – free, open-source materials for computer security classes

* [A collection of security related books by Hydragyrum](https://tinyurl.com/infosecbooks)


# Exploit Archives

* [Exploit Database](https://www.exploit-db.com/) – archive of exploits

* [NATIONAL VULNERABILITY DATABASE (NIST)](https://nvd.nist.gov/vuln)


# Awesome Security Curated Links Collection

*note: Some of the resources on the following curated lists maybe out of date*

* [Awesome Security](https://github.com/sbilly/awesome-security)
* [Awesome Web Security](https://github.com/qazbnm456/awesome-web-security)
* [Android-Security-Awesome](https://github.com/ashishb/android-security-awesome)
* [Awesome-Hacking](https://github.com/Hack-with-Github/Awesome-Hacking)
* [Awesome-Threat-Intelligence](https://github.com/hslatman/awesome-threat-intelligence)
* [Awesome Machine Learning for Cyber Security](https://github.com/jivoi/awesome-ml-for-cybersecurity)
* [Awesome Vehicle Security](https://github.com/jaredthecoder/awesome-vehicle-security)
* [Awesome-Web-Hacking](https://github.com/infoslack/awesome-web-hacking)
* [Awesome CTF](https://github.com/apsdehal/awesome-ctf)
* [Awesome-Windows-Kernel-Security-Development](https://github.com/ExpLife0011/awesome-windows-kernel-security-development)
* [Awesome-Mobile-Security](https://github.com/vaib25vicky/awesome-mobile-security)
* [Awesome AppSec](https://github.com/paragonie/awesome-appsec)
* [Awesome Sectalks - List of Sec talks/videos](https://github.com/PaulSec/awesome-sec-talks)
* [Awesome-Security-Hardening](https://github.com/decalage2/awesome-security-hardening)


More resources will be added later.
